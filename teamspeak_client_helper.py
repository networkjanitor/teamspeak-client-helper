import ts3


class TS3AuthenticationFailed(ts3.TS3Error):
    def __str__(self):
        return 'Authentication failed - maybe endpoint is not up yet?'


class TS3Helper:
    # TODO: Implement transition for mode/handler: https://github.com/pytransitions/transitions
    def __init__(self, *, host="localhost", port="25639", apikey):
        self._ts3conn = ts3.query.TS3Connection(host, port)
        try:
            self._ts3conn.auth(apikey=apikey)
        except ts3.query.TS3QueryError as e:
            print("Authentication failed:", e.resp.error["msg"])
            raise TS3AuthenticationFailed()

    def sendtextmessage(self, cmd_in):
        if len(cmd_in) == 0:
            return
        if self.verify_action('##### Do you really want to send that ? #####\n{}\n{}'.format(cmd_in,'#'*45)):
            message = cmd_in # this will probably change if you add params
            self._ts3conn.sendtextmessage(targetmode=2, target=0, msg=message)

    def verify_action(self, message):
        print(message)
        while True:
            verify = input('({whoami} || verify-action [y/n])> '.format(whoami=self.whoami()))
            if verify.lower() == 'y':
                return True
            elif verify.lower() == 'n':
                return False

    def whoami(self):
        # TODO: fix this mess, implement more useful functions into py-ts3 to get channel- and server name.
        # TODO: Should return in the form of [username@servername:channelname]
        result = self._ts3conn.whoami()
        nickname= self._ts3conn.clientvariable(result[0]['clid'], "client_nickname")
        description  = self._ts3conn.clientvariable(result[0]['clid'], "client_description")
        return '[{} ~ {}]'.format(nickname[0]['client_nickname'], description[0]['client_description'])

    def run(self):
        try:
            while True:
                cmd_in = input('(sendtextmessage)> ')
                self.sendtextmessage(cmd_in)
        except KeyboardInterrupt:
            print()
            exit()


if __name__ == '__main__':
    TS3Helper(apikey='KTYJ-DNRH-1X5E-RM4O-E0IW-ATKI').run()
