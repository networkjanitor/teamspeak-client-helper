Teamspeak3 Helper
=================

Goddamnit Teamspeak ....

Don't know why (some encoding mystery probably), but sometimes the chat message size doesn't let you post messages ... which you can actually post via ClientQuery.
So I wrote this helper to let me post long messages. 


Installation
------------

Requires the patched py-ts3 package, either from the wheel included in this repo or from cloning and building https://gitlab.com/networkjanitor/py-ts3